package com.wcu.hackathon.controller;

import com.wcu.hackathon.entity.Employee;
import com.wcu.hackathon.repository.EmployeeRepository;
import com.wcu.hackathon.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping("/{id}")
    ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(employeeService.getEmployeeById(id));
    }

    @GetMapping
    ResponseEntity<List<Employee>> getAllEmployee() {
        return ResponseEntity.ok(employeeService.getAllEmployee());
    }

    @GetMapping("/")
    ResponseEntity<Employee> getEmployeeByTitle(@RequestParam("jobTitle") String jobTitle){
        return ResponseEntity.ok(employeeService.getEmployeeByTitle(jobTitle));
    }
}
