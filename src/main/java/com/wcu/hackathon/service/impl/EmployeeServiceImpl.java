package com.wcu.hackathon.service.impl;

import com.wcu.hackathon.entity.Employee;
import com.wcu.hackathon.repository.EmployeeRepository;
import com.wcu.hackathon.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    @Override
    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found id: " + id));
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeByTitle(String jobTitle) {
        return employeeRepository.getEmployeesByJobTitle(jobTitle)
                .orElseThrow(() -> new RuntimeException("User not found job title: " + jobTitle));
    }
}
