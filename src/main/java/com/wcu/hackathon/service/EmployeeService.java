package com.wcu.hackathon.service;

import com.wcu.hackathon.entity.Employee;

import java.util.List;

public interface EmployeeService {
    Employee getEmployeeById(Long id);
    List<Employee> getAllEmployee();
    Employee getEmployeeByTitle(String jobTitle);
}
