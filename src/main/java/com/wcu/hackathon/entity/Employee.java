package com.wcu.hackathon.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "EMP")
public class Employee {
    @Id
    private Long employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String jobTitle;
    private LocalDateTime hireDate;
    private Integer managerId;

}
